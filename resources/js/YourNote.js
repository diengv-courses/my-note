// Chuyển table

var buttonShow = document.querySelector(".toggle-1");
var list1 = document.querySelector(".list-sub");
var igrid = document.querySelector(".i-grid");

var isListVisible = false;

buttonShow.addEventListener("click", function() {
    if (isListVisible) {
        // Nếu list1 đang hiển thị, ẩn list1 và hiển thị igrid
        list1.style.display = "none";
        igrid.style.display = "block";
        isListVisible = false; // Cập nhật trạng thái của biến flag
    } else {
        // Nếu list1 đang ẩn, hiển thị list1 và ẩn igrid
        list1.style.display = "block";
        igrid.style.display = "none";
        isListVisible = true; // Cập nhật trạng thái của biến flag
    }
});

// Sử lý modal

var modal = document.querySelector(".modal");
var view = document.querySelector(".new-note1");
var modal1 = document.querySelector(".modal-view");

var isVisible = true;
view.addEventListener("click", function () {
    if (isVisible) {
        modal.style.display = "block";
        isListVisible = false;
    } else {
        modal.style.display = "none";
        isListVisible = true;
    }
});

modal.addEventListener("click", function () {
    modal.style.display = "none";
});


const myModal = document.getElementById('myModal')
const myInput = document.getElementById('myInput')

myModal.addEventListener('shown.bs.modal', () => {
    myInput.focus()
})
