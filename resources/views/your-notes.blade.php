<x-master-layout>
    <div class=" bg-light">
       <div class="title-your-note p-4">
           <h3>Your Note</h3>
       </div>
        <div class="your-note-item ml-3">
            <ul class="nav nav-pills mx-4 " id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Home</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link " id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</button>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="modal-your-note p-3">
                        <div style="" class="row d-flex  justify-content-between ">
                            <div class=" btn btn-outline-secondary col position-relative  border-bottom border-5 border-danger m-3 border-0 shadow-sm rounded-2">
                                <div class="modal-icon row mt-3 ">
                                    <div class="col-lg-6 d-flex justify-content-start icon-modal-left">
                                        <i class="las la-birthday-cake icon-border-danger border-1 text-primary fs-5 border rounded-2 p-3"></i>
                                    </div>
                                    <div class="col-lg-6 d-flex justify-content-end icon-modal-right">
                                        <i class="las la-thumbtack fs-5"></i>
                                        <div class="dropdown">
                                <span class="" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                   <i class="las la-ellipsis-h fs-5"></i>
                                </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#">Action</a></li>
                                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-content  pt-3 pb-1 mb-3">
                                    <h4 class="mb-3">Birthday Celebration</h4>
                                    <p>You can easily share via message, WhatsApp, emails etc. You can also save your notes and edit it later or can easily delete the note.</p>


                                </div>
                                <div class="modal-footer  row pb-4 align-text-bottom p-2">
                                    <div style="" class="modal-footer-left col-lg-6 d-flex justify-content-start position-absolute start-0 ">
                                        <a href="#" class="text-decoration-none"><i class="las la-user-friends m-2 fs-5"></i><span class="text-dark fw-bold">Only Me</span> </a>
                                    </div>
                                    <div class="modal-footer-right col-lg-6 d-flex justify-content-end position-absolute ">
                                        <a href="#" class="text-decoration-none"><i class="las la-calendar m-2 fs-5"></i><span  class="text-dark fw-bold" id="note-reminder-date">01 Jan 2021</span></a>
                                    </div>
                                </div>

                            </div>
                            <div   class="btn btn-outline-secondary col position-relative  border-bottom border-5 border-danger m-3 border-0 shadow-sm rounded-2">
                                <div class="modal-icon row mt-3 ">
                                    <div class="col-lg-6 d-flex justify-content-start icon-modal-left">
                                        <i class="las la-birthday-cake icon-border-danger border-1 text-primary fs-5 border rounded-2 p-3"></i>
                                    </div>
                                    <div class="col-lg-6 d-flex justify-content-end icon-modal-right">
                                        <i class="las la-thumbtack fs-5"></i>
                                        <div class="dropdown">
                                <span class="" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                   <i class="las la-ellipsis-h fs-5"></i>
                                </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#">Action</a></li>
                                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-content pt-3 pb-1 mb-3">
                                    <h4 class="mb-3">Birthday Celebration</h4>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Default checkbox
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                                        <label class="form-check-label" for="flexCheckChecked">
                                            Checked checkbox
                                        </label>
                                    </div>

                                </div>
                                <div class="modal-footer  row pb-4 align-text-bottom p-2">
                                    <div style="" class="modal-footer-left col-lg-6 d-flex justify-content-start position-absolute start-0 ">
                                        <a href="#" class="text-decoration-none"><i class="las la-user-friends m-2 fs-5"></i><span class="text-dark fw-bold">Only Me</span> </a>
                                    </div>
                                    <div class="modal-footer-right col-lg-6 d-flex justify-content-end position-absolute ">
                                        <a href="#" class="text-decoration-none"><i class="las la-calendar m-2 fs-5"></i><span  class="text-dark fw-bold" id="note-reminder-date">01 Jan 2021</span></a>
                                    </div>
                                </div>

                            </div>
                            <div class="btn btn-outline-secondary col position-relative  border-bottom border-5 border-danger m-3 border-0 shadow-sm rounded-2">
                                <div class="modal-icon row mt-3 ">
                                    <div class="col-lg-6 d-flex justify-content-start icon-modal-left">
                                        <i class="las la-birthday-cake icon-border-danger border-1 text-primary fs-5 border rounded-2 p-3"></i>
                                    </div>
                                    <div class="col-lg-6 d-flex justify-content-end icon-modal-right">
                                        <i class="las la-thumbtack fs-5"></i>
                                        <div class="dropdown">
                                <span class="" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                   <i class="las la-ellipsis-h fs-5"></i>
                                </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#">Action</a></li>
                                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-content pt-3 pb-1 mb-3">
                                    <h4 class="mb-3">Image</h4>
                                    <div class="container mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <img width="110px" height="120px" class="rounded-4 " src="https://lbm.vn/wp-content/uploads/2013/05/anh-sang-ben-01.jpg" alt="">
                                            </div>
                                            <div class="col">
                                                <img width="110px" height="120px" class="rounded-4" src="https://lbm.vn/wp-content/uploads/2013/05/anh-sang-ben-01.jpg" alt="">
                                            </div>
                                            <div class="col">
                                                <img width="110px" height="120px" class="rounded-4" src="https://lbm.vn/wp-content/uploads/2013/05/anh-sang-ben-01.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>

                                </div><div class="modal-footer  row pb-4 align-text-bottom p-2">
                                    <div style="" class="modal-footer-left col-lg-6 d-flex justify-content-start position-absolute start-0 ">
                                        <a href="#" class="text-decoration-none"><i class="las la-user-friends m-2 fs-5"></i><span class="text-dark fw-bold">Only Me</span> </a>
                                    </div>
                                    <div class="modal-footer-right col-lg-6 d-flex justify-content-end position-absolute ">
                                        <a href="#" class="text-decoration-none"><i class="las la-calendar m-2 fs-5"></i><span  class="text-dark fw-bold" id="note-reminder-date">01 Jan 2021</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="modal-your-note p-3">
                        <div class="row d-flex  justify-content-between">
                            <div  class="col border m-3 rounded-2 bg-white border-0 ">
                                <div class="modal-icon row mt-3 ">
                                    <div class="col-lg-6 d-flex justify-content-start icon-modal-left">
                                        <i class="las la-birthday-cake icon-border-danger text-primary fs-4 border rounded-2 p-3"></i>
                                    </div>
                                    <div class="col-lg-6 d-flex justify-content-end icon-modal-right">
                                        <i class="las la-thumbtack fs-5"></i>
                                        <div class="dropdown">
                                <span class="" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                   <i class="las la-ellipsis-h fs-5"></i>
                                </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#">Action</a></li>
                                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-content pt-2 pb-1">
                                    <h4 class="mb-5">Birthday Celebration</h4>
                                    <p>You can easily share via message, WhatsApp, emails etc. You can also save your notes and edit it later or can easily delete the note.</p>


                                </div>
                                <div class="modal-footer row pb-4">
                                    <div class="modal-footer-left col-lg-6 d-flex justify-content-start">
                                        <a href="#" class="text-decoration-none"><i class="las la-user-friends m-2 fs-5"></i><span class="text-dark fw-bold">Only Me</span> </a>
                                    </div>
                                    <div class="modal-footer-right col-lg-6 d-flex justify-content-end">
                                        <a href="#" class="text-decoration-none"><i class="las la-calendar m-2 fs-5"></i><span  class="text-dark fw-bold" id="note-reminder-date">01 Jan 2021</span></a>
                                    </div>
                                </div>

                            </div>
                            <div class="col border m-3 rounded-2 bg-white border-0">
                                <div class="modal-icon row mt-3 ">
                                    <div class="col-lg-6 d-flex justify-content-start icon-modal-left">
                                        <i class="las la-birthday-cake icon-border-danger text-primary fs-4 border rounded-2 p-3"></i>
                                    </div>
                                    <div class="col-lg-6 d-flex justify-content-end icon-modal-right">
                                        <i class="las la-thumbtack fs-5"></i>
                                        <div class="dropdown">
                                <span class="" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                   <i class="las la-ellipsis-h fs-5"></i>
                                </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#">Action</a></li>
                                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col border m-3 rounded-2 bg-white border-0">
                                <div class="modal-icon row mt-3 ">
                                    <div class="col-lg-6 d-flex justify-content-start icon-modal-left">
                                        <i class="las la-birthday-cake icon-border-danger text-primary fs-4 border rounded-2 p-3"></i>
                                    </div>
                                    <div class="col-lg-6 d-flex justify-content-end icon-modal-right">
                                        <i class="las la-thumbtack fs-5"></i>
                                        <div class="dropdown">
                                <span class="" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                   <i class="las la-ellipsis-h fs-5"></i>
                                </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#">Action</a></li>
                                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <div class="modal-your-note p-3">
                        <div class="row d-flex  justify-content-between">
                            <div  class="col border m-3 rounded-2 bg-white border-0 ">
                                <div class="modal-icon row mt-3 ">
                                    <div class="col-lg-6 d-flex justify-content-start icon-modal-left">
                                        <i class="las la-birthday-cake icon-border-danger text-primary fs-4 border rounded-2 p-3"></i>
                                    </div>
                                    <div class="col-lg-6 d-flex justify-content-end icon-modal-right">
                                        <i class="las la-thumbtack fs-5"></i>
                                        <div class="dropdown">
                                <span class="" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                   <i class="las la-ellipsis-h fs-5"></i>
                                </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#">Action</a></li>
                                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-content pt-2 pb-1">
                                    <h4 class="mb-5">Birthday Celebration</h4>
                                    <p>You can easily share via message, WhatsApp, emails etc. You can also save your notes and edit it later or can easily delete the note.</p>


                                </div>
                                <div class="modal-footer row pb-4">
                                    <div class="modal-footer-left col-lg-6 d-flex justify-content-start">
                                        <a href="#" class="text-decoration-none"><i class="las la-user-friends m-2 fs-5"></i><span class="text-dark fw-bold">Only Me</span> </a>
                                    </div>
                                    <div class="modal-footer-right col-lg-6 d-flex justify-content-end">
                                        <a href="#" class="text-decoration-none"><i class="las la-calendar m-2 fs-5"></i><span  class="text-dark fw-bold" id="note-reminder-date">01 Jan 2021</span></a>
                                    </div>
                                </div>

                            </div>
                            <div class="col border m-3 rounded-2 bg-white border-0">
                                <div class="modal-icon row mt-3 ">
                                    <div class="col-lg-6 d-flex justify-content-start icon-modal-left">
                                        <i class="las la-birthday-cake icon-border-danger text-primary fs-4 border rounded-2 p-3"></i>
                                    </div>
                                    <div class="col-lg-6 d-flex justify-content-end icon-modal-right">
                                        <i class="las la-thumbtack fs-5"></i>
                                        <div class="dropdown">
                                <span class="" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                   <i class="las la-ellipsis-h fs-5"></i>
                                </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#">Action</a></li>
                                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col border m-3 rounded-2 bg-white border-0">
                                <div class="modal-icon row mt-3 ">
                                    <div class="col-lg-6 d-flex justify-content-start icon-modal-left">
                                        <i class="las la-birthday-cake icon-border-danger text-primary fs-4 border rounded-2 p-3"></i>
                                    </div>
                                    <div class="col-lg-6 d-flex justify-content-end icon-modal-right">
                                        <i class="las la-thumbtack fs-5"></i>
                                        <div class="dropdown">
                                <span class="" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                   <i class="las la-ellipsis-h fs-5"></i>
                                </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#">Action</a></li>
                                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</x-master-layout>
