
<x-guest-layout>
<div class="wrapper">
    <section class="login-content">
        <div class="container h-100">
            <div class="row justify-content-center align-items-center height-self-center pt-5">
                <div class="col-md-5 col-sm-12 col-12 rounded align-self-center bg-light shadow-lg">
                    <div class="sign-user_card">
                        <div class="logo-detail text-center">
                            <div class="d-flex align-items-center justify-content-center"><img
                                    src="https://templates.iqonic.design/note-plus/html/assets/images/logo.png"
                                    width="30px" class="img-fluid rounded-normal light-logo logo" alt="logo">
                                <h4 class="logo-title ml-3 mb-0 "style="padding: 20px">NotePlus</h4></div>
                        </div>
                        <div class="text-center">
                            <h3 class="mb-2">Sign Up</h3>
                            <p style="opacity: 0.7">Create your account.</p>
                        </div>
                        <form>
                            <div class="row px-3">
                                <div class="col-lg-6">
                                    <div class="floating-label form-group">
                                        <input class="floating-input form-control" type="text" placeholder="Fullname ">
                                        <label></label>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="floating-label form-group">
                                        <input class="floating-input form-control" type="text" placeholder=" Lastname">
                                        <label></label>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="floating-label form-group">
                                        <input class="floating-input form-control" type="text" placeholder="Email ">
                                        <label></label>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="floating-label form-group">
                                        <input class="floating-input form-control" type="text" placeholder=" Password">
                                        <label></label>
                                    </div>
                                </div><div class="col-lg-6">
                                    <div class="floating-label form-group">
                                        <input class="floating-input form-control" type="text" placeholder=" Confirm Password">
                                        <label></label>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-check mb-3 text-left " >
                                        <input type="checkbox" class="form-check-input rounded-circle" id="customCheck1">
                                        <label class="form-check-label" for="customCheck1" style="opacity: 0.7">
                                            I agree with the terms of use
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center" style="padding: 20px">
                                <button type="submit" class="btn btn-dark text-decoration-none">Sign Up</button>
                                <p class="mt-3 mb-0">
                                    Already have an Account <a  href="" class="text-dark text-decoration-none"><b>Sign In</b></a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</x-guest-layout>
