
<x-guest-layout>
<div class="wrapper">
    <section class="login-content">
        <div class="container h-100">
            <div class="row justify-content-center align-items-center height-self-center pt-5">
                <div class="col-md-5 col-sm-12 col-12 align-self-center bg-light rounded shadow-lg">
                    <div class="sign-user_card">
                        <div class="logo-detail text-center">
                            <div class="d-flex align-items-center justify-content-center"><img
                                    src="https://templates.iqonic.design/note-plus/html/assets/images/logo.png"
                                    width="30px" class="img-fluid rounded-normal light-logo logo" alt="logo">
                                <h4 class="logo-title ml-3 mb-0 "style="padding: 20px">NotePlus</h4></div>
                        </div>
                        <div class="text-center">
                            <h3 class="mb-2">Success !</h3>
                            <p style="opacity: 0.7">A email has been send to youremail@domain.com. Please check for an email from company and click on the included link to reset your password.</p>
                        </div>
                        <form>
                            <div class="text-center" style="padding: 20px" >
                                <button type="submit" class="btn btn-dark text-decoration-none">Back to Home</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</x-guest-layout>
