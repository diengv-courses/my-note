<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite(['resources/scss/app.scss', 'resources/js/app.js'])

</head>
<body>
<div class="container-md text-centen" style="background-color: #e2e8f0">
        <div class="row gx-5" style="background-color: white">
            <div class="col-3">
                <div class="p-3 border bg-light bg-white text-dark">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">Add New User</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <form>
                            <div class="form-group text-center">
                                <div class="d-flex justify-content-center">
                                    <div class="crm-profile-img-edit">
                                        <img style="width: 50px; height: 50px" class="crm-profile-pic avatar-100" src="https://cdn3.vectorstock.com/i/1000x1000/57/42/person-avatar-user-icon-vector-14625742.jpg">
                                        <div class="crm-p-image bg-primary">
                                            <i class="las la-pen upload-button"></i>

                                        </div>
                                    </div>
                                </div>
                                <div class="img-extension mt-3">
                                    <div class="d-inline-block align-items-center">
                                        <span>Only</span>
                                        <a href="javascript:void();">.jpg</a>
                                        <a href="javascript:void();">.png</a>
                                        <a href="javascript:void();">.jpeg</a>
                                        <span>allowed</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <br>
                                <label>User Role:</label>
                                <br>
                                <br>
                                <select class="form-control" id="selectuserrole">
                                    <option>Select</option>
                                    <option>Web Designer</option>
                                    <option>Web Developer</option>
                                    <option>Tester</option>
                                    <option>Php Developer</option>
                                    <option>Ios Developer </option>
                                </select>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="furl">Facebook Url:</label>
                                <br>
                                <br>
                                <input type="text" class="form-control" id="furl" placeholder="Facebook Url">
                            </div>
                            <br>

                            <div class="form-group">
                                <label for="turl">Twitter Url:</label>
                                <br>
                                <br>
                                <input type="text" class="form-control" id="turl" placeholder="Twitter Url">
                            </div>
                            <br>

                            <div class="form-group">
                                <label for="instaurl">Instagram Url:</label>
                                <br>
                                <br>
                                <input type="text" class="form-control" id="instaurl" placeholder="Instagram Url">
                            </div>
                            <br>

                            <div class="form-group">
                                <label for="lurl">Linkedin Url:</label>
                                <br>
                                <br>
                                <input type="text" class="form-control" id="lurl" placeholder="Linkedin Url">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="p-3 border bg-light bg-white text-dark">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">New User Information</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <div class="new-user-info bg-light text-dark" >
                            <form>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="fname">First Name:</label>
                                        <br>
                                        <br>
                                        <input type="text" class="form-control" id="fname" placeholder="First Name">
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-md-6">
                                        <label for="lname">Last Name:</label>
                                        <br>
                                        <br>
                                        <input type="text" class="form-control" id="lname" placeholder="Last Name">
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-md-6">
                                        <br>
                                        <label for="add1">Street Address 1:</label>
                                        <br>
                                        <br>
                                        <input type="text" class="form-control" id="add1" placeholder="Street Address 1">
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-md-6">
                                        <br>
                                        <label for="add2">Street Address 2:</label>
                                        <br>
                                        <br>
                                        <input type="text" class="form-control" id="add2" placeholder="Street Address 2">
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-md-12">
                                        <br>
                                        <label for="cname">Company Name:</label>
                                        <br>
                                        <br>
                                        <input type="text" class="form-control" id="cname" placeholder="Company Name">
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-sm-12">
                                        <br>
                                        <label>Country:</label>
                                        <br>
                                        <br>
                                        <select class="form-control" id="selectcountry">
                                            <option>Select Country</option>
                                            <option>Caneda</option>
                                            <option>Noida</option>
                                            <option>USA</option>
                                            <option>India</option>
                                            <option>Africa</option>
                                        </select>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-md-6">
                                        <br>
                                        <label for="mobno">Mobile Number:</label>
                                        <br>
                                        <br>
                                        <input type="text" class="form-control" id="mobno" placeholder="Mobile Number">
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-md-6">
                                        <br>
                                        <label for="altconno">Alternate Contact:</label>
                                        <br>
                                        <br>
                                        <input type="text" class="form-control" id="altconno" placeholder="Alternate Contact">
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-md-6">
                                        <br>
                                        <label for="email">Email:</label>
                                        <br>
                                        <br>
                                        <input type="email" class="form-control" id="email" placeholder="Email">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <br>
                                        <label for="pno">Pin Code:</label>
                                        <br>
                                        <br>
                                        <input type="text" class="form-control" id="pno" placeholder="Pin Code">
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-md-12">
                                        <br>
                                        <label for="city">Town/City:</label>
                                        <br>
                                        <br>
                                        <input type="text" class="form-control" id="city" placeholder="Town/City">
                                    </div>
                                    <br>
                                    <br>
                                </div>
                                <hr>
                                <h5 class="mb-3">Security</h5>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="uname">User Name:</label>
                                        <br>
                                        <br>
                                        <input type="text" class="form-control" id="uname" placeholder="User Name">
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-md-6">
                                        <br>
                                        <label for="pass">Password:</label>
                                        <br>
                                        <br>
                                        <input type="password" class="form-control" id="pass" placeholder="Password">
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group col-md-6">
                                        <br>
                                        <label for="rpass">Repeat Password:</label>
                                        <br>
                                        <br>
                                        <input type="password" class="form-control" id="rpass" placeholder="Repeat Password ">
                                    </div>
                                    <br>
                                    <br>
                                </div>
                                <div class="checkbox">
                                    <br>
                                    <label> <input class="mr-2" type="checkbox">Enable Two-Factor-Authentication</label>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-dark">Add New User</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</body>
</html>
