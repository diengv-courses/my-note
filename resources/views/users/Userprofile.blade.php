<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite(['resources/scss/app.scss', 'resources/js/app.js'])
    <style>
        h5{
            margin-left: 30px;
        }
        p{
            margin-left: 30px;
        }
    </style>
</head>
<body>
<div class="container-md text-centen" style="background-color: #e2e8f0" >

<div class="row">
    <img style="height: 500px;" src="https://baothainguyen.vn/file/oldimage/baothainguyen/UserFiles/image/d25.jpg">
</div>

    <div style="padding-top: 30px ; ">
        <div class="row gx-5">
            <div class="col">
                <div class="p-3 border bg-light" style="height: 90%; border-radius: 10px ">
                    <h5 >Personal Profile</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                     </p>
                </div>

            </div>
            <div class="col">
                <div class="p-3 border bg-light" style=" border-radius: 10px">
                    <i style="margin-left: 90px; color: deepskyblue" class="fa-solid fa-medal fa-4x"></i>
                    <p class="text-center" style="font-size: 30px ">42+</p>
                    <h5 class="text-center" style="font-size: 30px ">Awards</h5>
                </div>
            </div>
            <div class="col">
                <div class="p-3 border bg-light" style=" border-radius: 10px">
                    <i style="margin-left: 90px; color: limegreen"  class="fa-solid fa-laptop-code fa-4x"></i>
                    <p class="text-center" style="font-size: 30px ">3 + year</p>
                    <h5 class="text-center" style="font-size: 30px ">Experience</h5>
                </div>
            </div>
            <div class="col">
                <div class="p-3 border bg-light" style=" border-radius: 10px">
                    <i style="margin-left: 90px; color: hotpink" class="fa-regular fa-hand fa-4x"></i>
                    <p class="text-center" style="font-size: 30px ">424+</p>
                    <h5 class="text-center" style="font-size: 30px ">Participated</h5>
                </div>
            </div>

        </div>
    </div>
    <div class="row g-0" style="padding-top: 30px">
        <div class="col-sm-6 col-md-6">
            <div class="col">
                <div class="p-3 border bg-light" style=" border-radius: 10px">
                    <h5>
                        Education Traning
                    </h5>
                    <hr>
                    <ul class="list-inline p-0 m-0">
                        <li class="d-flex align-items-center mb-3">
                            <div class="profile-icon iq-icon-box rounded-small bg-danger-light text-center">
                                <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                        <path d="M23.3379 5.745L12.8777 2.53125C12.3077 2.35612 11.6927 2.35612 11.1231 2.53125L0.662429 5.745C-0.220321 6.01612 -0.220321 7.18349 0.662429 7.45462L2.48605 8.01487C2.08593 8.50949 1.83993 9.11287 1.81555 9.77362C1.45443 9.98062 1.20018 10.3541 1.20018 10.8C1.20018 11.2042 1.41318 11.5444 1.71993 11.7619L0.762554 16.0699C0.679304 16.4445 0.964304 16.8 1.34793 16.8H3.45205C3.83605 16.8 4.12105 16.4445 4.0378 16.0699L3.08043 11.7619C3.38718 11.5444 3.60018 11.2042 3.60018 10.8C3.60018 10.3661 3.35755 10.0031 3.01293 9.79237C3.04143 9.22912 3.32943 8.73112 3.7888 8.41537L11.1227 10.6687C11.4624 10.773 12.1142 10.9031 12.8773 10.6687L23.3379 7.45499C24.2211 7.1835 24.2211 6.0165 23.3379 5.745V5.745ZM13.2298 11.8159C12.1599 12.1444 11.2483 11.9629 10.7702 11.8159L5.33193 10.1452L4.80018 14.4C4.80018 15.7256 8.02368 16.8 12.0002 16.8C15.9767 16.8 19.2002 15.7256 19.2002 14.4L18.6684 10.1449L13.2298 11.8159V11.8159Z" fill="#FF9BBD"></path>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="24" height="19.2" fill="white"></rect>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="pl-3">
                                <h5>South Kellergrove Junior</h5>
                                <p class="mb-0">Junior High School | Class of 2008</p>
                            </div>
                        </li>
                        <li class="d-flex align-items-center mb-3">
                            <div class="profile-icon iq-icon-box rounded-small bg-info-light text-center">
                                <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M17.1107 17.1746L12 22.2853L6.88929 17.1746C3.05893 17.3406 0 20.4746 0 24.3424V24.8567C0 26.2764 1.15179 27.4281 2.57143 27.4281H21.4286C22.8482 27.4281 24 26.2764 24 24.8567V24.3424C24 20.4746 20.9411 17.3406 17.1107 17.1746ZM0.728571 4.27457L1.07143 4.35493V7.4835C0.696429 7.7085 0.428571 8.09957 0.428571 8.571C0.428571 9.021 0.675 9.396 1.02321 9.62636L0.1875 12.9639C0.0964286 13.3335 0.3 13.7139 0.594643 13.7139H2.83393C3.12857 13.7139 3.33214 13.3335 3.24107 12.9639L2.40536 9.62636C2.75357 9.396 3 9.021 3 8.571C3 8.09957 2.73214 7.7085 2.35714 7.4835V4.66564L5.89286 5.51743C5.43214 6.43886 5.14286 7.46743 5.14286 8.571C5.14286 12.3585 8.2125 15.4281 12 15.4281C15.7875 15.4281 18.8571 12.3585 18.8571 8.571C18.8571 7.46743 18.5732 6.43886 18.1071 5.51743L23.2661 4.27457C24.2411 4.03886 24.2411 2.82278 23.2661 2.58707L13.0661 0.122784C12.3696 -0.0432879 11.6357 -0.0432879 10.9393 0.122784L0.728571 2.58171C-0.241071 2.81743 -0.241071 4.03886 0.728571 4.27457Z" fill="#88DFFF"></path>
                                </svg>
                            </div>
                            <div class="pl-3">
                                <h5>Milchuer College</h5>
                                <p class="mb-0">Master of Science in Computer Science | 2015</p>
                            </div>
                        </li>
                        <li class="d-flex align-items-center mb-3">
                            <div class="profile-icon iq-icon-box rounded-small bg-warning-light text-center">
                                <svg width="24" height="26" viewBox="0 0 24 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M23.25 6.46152V7.26922C23.25 7.37632 23.2105 7.47904 23.1402 7.55478C23.0698 7.63051 22.9745 7.67306 22.875 7.67306H21.75V8.27883C21.75 8.61337 21.4981 8.8846 21.1875 8.8846H2.8125C2.50186 8.8846 2.25 8.61337 2.25 8.27883V7.67306H1.125C1.02554 7.67306 0.930161 7.63051 0.859835 7.55478C0.789509 7.47904 0.75 7.37632 0.75 7.26922V6.46152C0.750001 6.38163 0.772007 6.30353 0.813234 6.2371C0.854462 6.17068 0.913058 6.11892 0.981609 6.08837L11.8566 1.64606C11.9484 1.60512 12.0516 1.60512 12.1434 1.64606L23.0184 6.08837C23.0869 6.11892 23.1455 6.17068 23.1868 6.2371C23.228 6.30353 23.25 6.38163 23.25 6.46152V6.46152ZM22.125 21.8077H1.875C1.25367 21.8077 0.75 22.3501 0.75 23.0192V23.8269C0.75 23.934 0.789509 24.0367 0.859835 24.1125C0.930161 24.1882 1.02554 24.2308 1.125 24.2308H22.875C22.9745 24.2308 23.0698 24.1882 23.1402 24.1125C23.2105 24.0367 23.25 23.934 23.25 23.8269V23.0192C23.25 22.3501 22.7463 21.8077 22.125 21.8077ZM4.5 9.69229V19.3846H2.8125C2.50186 19.3846 2.25 19.6558 2.25 19.9904V21H21.75V19.9904C21.75 19.6558 21.4981 19.3846 21.1875 19.3846H19.5V9.69229H16.5V19.3846H13.5V9.69229H10.5V19.3846H7.5V9.69229H4.5Z" fill="#FFCB99"></path>
                                </svg>
                            </div>
                            <div class="pl-3">
                                <h5>Beechtown University</h5>
                                <p class="mb-0">Bachelor of Science in Computer Science | 2013</p>
                            </div>
                        </li>
                        <li class="d-flex align-items-center mb-3">
                            <div class="profile-icon iq-icon-box rounded-small bg-success-light text-center">
                                <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                        <path d="M0 8.39995V18.5999C0 18.9314 0.2685 19.1999 0.6 19.1999H3.6V7.19994H1.2C0.537375 7.19994 0 7.73732 0 8.39995ZM13.5 6.59994H12.6V5.09994C12.6 4.93419 12.4658 4.79994 12.3 4.79994H11.7C11.5343 4.79994 11.4 4.93419 11.4 5.09994V7.49994C11.4 7.66569 11.5343 7.79995 11.7 7.79995H13.5C13.6658 7.79995 13.8 7.66569 13.8 7.49994V6.89994C13.8 6.73419 13.6658 6.59994 13.5 6.59994ZM18.6656 4.20144L12.6656 0.201317C12.4685 0.0700041 12.2369 -6.10352e-05 12 -6.10352e-05C11.7631 -6.10352e-05 11.5315 0.0700041 11.3344 0.201317L5.33438 4.20144C5.17003 4.31101 5.03527 4.45945 4.94206 4.63358C4.84884 4.80772 4.80004 5.00218 4.8 5.19969V19.1999H9.6V13.7999C9.6 13.4684 9.8685 13.1999 10.2 13.1999H13.8C14.1315 13.1999 14.4 13.4684 14.4 13.7999V19.1999H19.2V5.20007C19.2 4.79882 18.9994 4.42382 18.6656 4.20144ZM12 9.59995C10.3433 9.59995 9 8.2567 9 6.59994C9 4.94319 10.3433 3.59994 12 3.59994C13.6568 3.59994 15 4.94319 15 6.59994C15 8.2567 13.6568 9.59995 12 9.59995ZM22.8 7.19994H20.4V19.1999H23.4C23.7315 19.1999 24 18.9314 24 18.5999V8.39995C24 7.73732 23.4626 7.19994 22.8 7.19994Z" fill="#81F4B2"></path>
                                    </g>
                                    <defs>
                                        <clipPath id="clip1">
                                            <rect width="24" height="19.2" fill="white"></rect>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="pl-3">
                                <h5>South Kellergrove High</h5>
                                <p class="mb-0">Senior High School | 2010</p>
                            </div>
                        </li>
                        <li class="d-flex align-items-center mb-2">
                            <div class="profile-icon iq-icon-box rounded-small bg-info-light text-center">
                                <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M17.1107 17.1746L12 22.2853L6.88929 17.1746C3.05893 17.3406 0 20.4746 0 24.3424V24.8567C0 26.2764 1.15179 27.4281 2.57143 27.4281H21.4286C22.8482 27.4281 24 26.2764 24 24.8567V24.3424C24 20.4746 20.9411 17.3406 17.1107 17.1746ZM0.728571 4.27457L1.07143 4.35493V7.4835C0.696429 7.7085 0.428571 8.09957 0.428571 8.571C0.428571 9.021 0.675 9.396 1.02321 9.62636L0.1875 12.9639C0.0964286 13.3335 0.3 13.7139 0.594643 13.7139H2.83393C3.12857 13.7139 3.33214 13.3335 3.24107 12.9639L2.40536 9.62636C2.75357 9.396 3 9.021 3 8.571C3 8.09957 2.73214 7.7085 2.35714 7.4835V4.66564L5.89286 5.51743C5.43214 6.43886 5.14286 7.46743 5.14286 8.571C5.14286 12.3585 8.2125 15.4281 12 15.4281C15.7875 15.4281 18.8571 12.3585 18.8571 8.571C18.8571 7.46743 18.5732 6.43886 18.1071 5.51743L23.2661 4.27457C24.2411 4.03886 24.2411 2.82278 23.2661 2.58707L13.0661 0.122784C12.3696 -0.0432879 11.6357 -0.0432879 10.9393 0.122784L0.728571 2.58171C-0.241071 2.81743 -0.241071 4.03886 0.728571 4.27457Z" fill="#88DFFF"></path>
                                </svg>
                            </div>
                            <div class="pl-3">
                                <h5>Milchuer College</h5>
                                <p class="mb-0">Master of Science in Computer Science | 2015</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-6 col-md-6" style="padding-left: 30px; border-radius: 10px">
            <div class="row gx-5">
                <div class="col">
                    <div class="p-3 border bg-light" style="border-radius: 10px">
                        <div class="card-header">
                            <div class="header-title">
                                <h4 class="card-title">Languages</h4>
                            </div>
                            <hr>
                            <div class="card-body">
                                <ul class="list-inline p-0 mb-0">
                                    <li>
                                        <div class="d-flex align-items-center justify-content-between mb-3 row">
                                            <div class="col-10">
                                                <div class="d-flex align-items-center">
                                                    <p class="mb-0 font-size-16 mr-3">English</p>
                                                    <div class="iq-progress-bar-linear d-inline-block iq-progress-height mt-1 w-80">
                                                        <div class="iq-progress-bar iq-bg-danger">
                                                            <span class="bg-danger" data-percent="75" style="transition: width 2s ease 0s; width: 75%;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="percentage float-right text-danger font-weight-bold">75%
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="d-flex align-items-center justify-content-between mb-3 row">
                                            <div class="col-10">
                                                <div class="d-flex align-items-center">
                                                    <p class="mb-0 font-size-16 mr-3">French</p>
                                                    <div class="iq-progress-bar-linear d-inline-block iq-progress-height mt-1 w-80">
                                                        <div class="iq-progress-bar iq-bg-primary">
                                                            <span class="bg-primary" data-percent="50" style="transition: width 2s ease 0s; width: 50%;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="percentage float-right text-primary font-weight-bold">50%
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="d-flex align-items-center justify-content-between row">
                                            <div class="col-10">
                                                <div class="d-flex align-items-center">
                                                    <p class="mb-0 font-size-16 mr-3">Spanish</p>
                                                    <div class="iq-progress-bar-linear d-inline-block iq-progress-height mt-1 w-80">
                                                        <div class="iq-progress-bar iq-bg-success">
                                                            <span class="bg-success" data-percent="65" style="transition: width 2s ease 0s; width: 65%;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="percentage float-right text-success font-weight-bold">65%
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="col">
                    <div class="p-3 border bg-light" style="border-radius: 10px">
                        <div class="card-header">
                            <div class="header-title">
                                <h4 class="card-title">Social</h4>
                            </div>
                            <hr>
                            <div class="card-body">
                                <ul class="list-inline p-0 m-0">
                                    <li class="mb-3 d-flex">
                                        <span>
                                            <i class="fa-brands fa-facebook"></i>
                                        </span>
                                        <p class="mb-0 font-size-16 line-height">fb.me/nataliedawson</p>
                                    </li>
                                    <li class="mb-3 d-flex">
                                        <span>
                                            <i class="fa-brands fa-twitter"></i>
                                        </span>
                                        <p class="mb-0 font-size-16 line-height">@nataliedawson</p>
                                    </li>
                                    <li class=" d-flex">
                                        <span>
                                            <i class="fa-brands fa-instagram"></i>
                                        </span>
                                        <p class="mb-0 font-size-16 line-height">@natalietweets</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col" style="padding-top: 30px">
                <div class="col-12">
                    <div class="p-3 border bg-light" style="border-radius: 10px">
                        <div class="card-header">
                            <div class="header-title">
                                <h4 class="card-title">Experience</h4>
                            </div>
                            <hr>
                            <div class="card-body">
                                <ul class="list-inline mb-0 p-0">
                                    <li class="d-flex align-items-center mb-3">
                                        <span style="background-color: lightskyblue; color: blue; width: 20px; border-radius: 5px" class="text-center">1</span>
                                        <p class="mb-0 font-size-16">Software Engineer at Mathica Labs | 2018 - present</p>
                                    </li>
                                    <li class="d-flex align-items-center">
                                        <span style="background-color: orange; color: orangered; width: 20px; border-radius: 5px" class="text-center">2</span>
                                        <p class="mb-0 font-size-16">Junior Software Engineer at Zimcore Solutions | 2015 - 2018</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

        <div class="row g-2" style="padding-top: 30px">
            <div class="col-4">
                <div class="p-3 border bg-light" style="border-radius: 10px; height: 100%">
                    <div class="card-header">
                        <div class="header-title">
                            <h4 class="card-title">Professional Skills</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <ul class="list-inline p-0 mb-0">
                            <li>
                                <div class="d-flex align-items-center justify-content-between mb-4 row" style="margin-top: -15px">
                                    <div class="col-lg-4">
                                        <p class="mb-0 font-size-16">Photoshop</p>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="iq-progress-bar bg-primary-light mt-2">
                                    <span class="bg-primary iq-progress progress-1" data-percent="85" style="transition: width 2s ease 0s; width: 85%;">
                                       <span class="progress-text-one bg-primary">85%</span>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="d-flex align-items-center justify-content-between mb-4 row">
                                    <div class="col-lg-4">
                                        <p class="mb-0 font-size-16">Illustrator</p>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="iq-progress-bar bg-warning-light mt-2">
                                    <span class="bg-warning iq-progress progress-1" data-percent="65" style="transition: width 2s ease 0s; width: 65%;">
                                       <span class="progress-text-one bg-warning">65%</span>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="d-flex align-items-center justify-content-between mb-4 row">
                                    <div class="col-lg-4">
                                        <p class="mb-0 font-size-16">Adobe XD</p>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="iq-progress-bar bg-success-light mt-2">
                                    <span class="bg-success iq-progress progress-1" data-percent="55" style="transition: width 2s ease 0s; width: 55%;">
                                       <span class="progress-text-one bg-success">55%</span>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="d-flex align-items-center justify-content-between row">
                                    <div class="col-lg-4">
                                        <p class="mb-0 font-size-16">Figma</p>
                                    </div>
                                    <div class="col-lg-8" style="margin-top: -15px">
                                        <div class="iq-progress-bar bg-info-light mt-2">
                                    <span class="bg-info iq-progress progress-1" data-percent="60" style="transition: width 2s ease 0s; width: 60%;">
                                       <span class="progress-text-one bg-info">60%</span>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="p-3 border bg-light" style="border-radius: 10px">
                    <div class="card-header">
                        <div class="header-title">
                            <h4 class="card-title">Personal Skills</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <ul class="list-inline p-0 m-0">
                            <li class="mb-3">
                                <div class="d-flex align-items-center">
                                    <div style="background-color: black;width: 20px; border-radius: 5px;color:white" class="text-center">1</div>
                                    <div class="ml-4">
                                        <p class="mb-0 font-size-16">Creative spirit</p>
                                    </div>
                                </div>
                            </li>
                            <li class="mb-3">
                                <div class="d-flex align-items-center">
                                    <div style="background-color: black;width: 20px; border-radius: 5px;color:white" class="text-center">2</div>
                                    <div class="ml-4">
                                        <p class="mb-0 font-size-16">Reliable and professional</p>
                                    </div>
                                </div>
                            </li>
                            <li class="mb-3">
                                <div class="d-flex align-items-center">
                                    <div style="background-color: black;width: 20px; border-radius: 5px;color:white" class="text-center">3</div>
                                    <div class="ml-4">
                                        <p class="mb-0 font-size-16">Time management</p>
                                    </div>
                                </div>
                            </li>
                            <li class="mb-3">
                                <div class="d-flex align-items-center">
                                    <div style="background-color: black;width: 20px; border-radius: 5px;color:white" class="text-center">4</div>
                                    <div class="ml-4">
                                        <p class="mb-0 font-size-16">Organized</p>
                                    </div>
                                </div>
                            </li>
                            <li class="">
                                <div class="d-flex align-items-center">
                                    <div style="background-color: black;width: 20px; border-radius: 5px;color:white" class="text-center">5</div>
                                    <div class="ml-4">
                                        <p class="mb-0 font-size-16">Organized</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-4">
                <div class="p-3 border bg-light" style="border-radius: 10px">
                    <div class="card-header">
                        <div class="header-title">
                            <h4 class="card-title">Contact Information</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <ul class="list-inline p-0 m-0 iq-contact-rest">
                            <li class="mb-3 d-flex">
                           <span><i class="mr-3">
                              <svg width="20" class="svg-icon" id="up-01" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                 <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" style="stroke-dasharray: 53, 73; stroke-dashoffset: 0;"></path>
                                 <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" style="stroke-dasharray: 19, 39; stroke-dashoffset: 0;"></path>
                              </svg>
                           </i></span>
                                <p class="mb-0 font-size-16 line-height">505 West Brickyard Rd, CA , USA</p>
                            </li>
                            <li class="mb-3 d-flex">
                           <span><i class="mr-3">
                              <svg width="20" class="svg-icon" id="up-02" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                 <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z" style="stroke-dasharray: 67, 87; stroke-dashoffset: 0;"></path>
                              </svg>
                           </i></span>
                                <p class="mb-0 font-size-16 line-height">+91 01234 56789</p>
                            </li>
                            <li class="mb-3 d-flex">
                           <span><i class="mr-3">
                              <svg width="20" class="svg-icon" id="up-03" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                 <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 19v-8.93a2 2 0 01.89-1.664l7-4.666a2 2 0 012.22 0l7 4.666A2 2 0 0121 10.07V19M3 19a2 2 0 002 2h14a2 2 0 002-2M3 19l6.75-4.5M21 19l-6.75-4.5M3 10l6.75 4.5M21 10l-6.75 4.5m0 0l-1.14.76a2 2 0 01-2.22 0l-1.14-.76" style="stroke-dasharray: 99, 119; stroke-dashoffset: 0;"></path>
                              </svg>
                           </i></span>
                                <p class="mb-0 font-size-16 line-height">john@property.com</p>
                            </li>
                            <li class="mb-3 d-flex">
                                <a href="javascript:void(0);" class="d-flex">
                                    <i class="mr-3">
                                        <svg width="20" class="svg-icon" id="up-04" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.828 10.172a4 4 0 00-5.656 0l-4 4a4 4 0 105.656 5.656l1.102-1.101m-.758-4.899a4 4 0 005.656 0l4-4a4 4 0 00-5.656-5.656l-1.1 1.1" style="stroke-dasharray: 53, 73; stroke-dashoffset: 0;"></path>
                                        </svg>
                                    </i>
                                    <p class="mb-0 font-size-16 line-height">http://www.yourwebsite.com </p>
                                </a>
                            </li>
                            <li class="d-flex">
                           <span><i class="mr-3">
                              <svg width="20" class="svg-icon" id="up-05" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                 <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" style="stroke-dasharray: 94, 114; stroke-dashoffset: 0;"></path>
                              </svg>
                           </i></span>
                                <p class="mb-0 font-size-16 line-height">9486 Roberts St.
                                    Monroe Township.</p>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>





</div>
</body>
</html>
