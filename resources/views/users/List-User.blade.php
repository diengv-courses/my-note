<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite(['resources/scss/app.scss', 'resources/js/app.js'])
</head>
<body>
<div class="container" >


        <div class="row g-2" >
            <div class="col-12">
                <div class="p-3 border bg-light bg-white text-dark">
                    <div class="card-body d-flex align-items-center">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-capitalize">Users List</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="row gy-5">
                        <div class="col-6">
                                <input type="search" class="form-control" id="exampleInputSearch" placeholder="Search" aria-controls="user-list-table">
                        </div>
                        <div class="col-6" style="display: flex ;padding-left: 350px;">
                            <div class="col-6 col-sm-4 text-center" style="background-color: black;width: 50px; color: white; border-radius: 10px; line-height: 35px">Print</div>
                            <div  class="col-6 col-sm-4 text-center" style="background-color: black;width: 50px; color: white; border-radius: 10px; line-height: 35px; margin-left: 30px">Excel</div>
                            <div  class="col-6 col-sm-4 text-center" style="background-color: black;width: 50px; color: white; border-radius: 10px; line-height: 35px; margin-left: 30px">Pdf</div>
                        </div>
                    </div>
                    <table id="user-list-table" class="table table-striped tbl-server-info mt-4" role="grid" aria-describedby="user-list-page-info">
                        <thead>
                        <tr class="ligth">
                            <th style="background-color: #1a202c ; color: white; border-radius: 10px 0 0 ">Profile</th>
                            <th style="background-color: #1a202c ; color: white">Name</th>
                            <th style="background-color: #1a202c ; color: white">Contact</th>
                            <th style="background-color: #1a202c ; color: white">Email</th>
                            <th style="background-color: #1a202c ; color: white">Country</th>
                            <th style="background-color: #1a202c ; color: white">Status</th>
                            <th style="background-color: #1a202c ; color: white">Company</th>
                            <th style="background-color: #1a202c ; color: white">Join Date</th>
                            <th style="min-width: 100px;background-color: #1a202c ; color: white; border-radius: 0 10px 0 0  ">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        for ($i = 1; $i <= 10; $i++) {
                            ?>
                        <tr>
                            <td ><img style="width: 30%; margin-right: 30px" class="rounded img-fluid avatar-40" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMktmRX04rbUrHE3tLjtrK1zE65l-WXjStOf-GjwU&s" alt="profile"></td>
                            <td>Anna Sthesia</td>
                            <td>(760) 756 7568</td>
                            <td>annasthesia@gmail.com</td>
                            <td>USA</td>
                            <td><span style="color: darkgray">Active</span></td>
                            <td>Acme Corporation</td>
                            <td>2019/12/01</td>
                            <td>
                                <div class="flex align-items-center list-user-action">
                                    <a style="color: #1a202c;opacity: 50%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add" ><i class="fa-solid fa-user-plus"></i></a>
                                    <a style="color: #1a202c;opacity: 50%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" ><i class="fa-solid fa-pencil"></i></a>
                                    <a style="color: #1a202c;opacity: 50%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa-regular fa-trash-can"></i></a>
                                </div>
                            </td>
                        </tr>
                            <?php
                        }
                        ?>


                        </tbody>

                    </table>
                    <div class="row justify-content-between mt-3">
                        <div id="user-list-page-info" class="col-md-6">
                            <span>Showing 1 to 5 of 5 entries</span>
                        </div>
                        <div class="col-md-6">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-end mb-0">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>


                </div>

            </div>

        </div>
        </div>
        </div>
        </div>
    </div>
    </div>


</body>
</html>
