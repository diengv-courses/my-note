<div>
    <!-- People find pleasure in different ways. I find it in keeping my mind clear. - Marcus Aurelius -->
    <div class=" header row d-flex">
        <!-- People find pleasure in different ways. I find it in keeping my mind clear. - Marcus Aurelius -->
        <div class="col-lg-10 bg-white rounded-3 mt-5 mr-3 p-4">
            <h4>{{__('Privacy Policy')}}</h4>
        </div>
        <div class=" col-lg-2 d-flex justify-content-end mt-5">
            <div class="header-icon bg-white rounded-start rounded-end d-flex justify-content-center align-items-center text-white w-75">
                <ul class=" m-0">
                    <li class="d-inline h-75">
                        <i class="las la-bell fs-4" data-bs-toggle="dropdown" id="dropdownMenuButton1" aria-expanded="false"></i>
                        <span class="badge bg-dark rounded-circle position-absolute top-0 end-0">2</span>
                        <ul class="dropdown-menu fs-5" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item " href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li>
                    <li class="d-inline">
                        <i class="las la-envelope fs-4" data-bs-toggle="dropdown" id="dropdownMenuButton2" aria-expanded="false"></i>
                        <span class="badge bg-dark rounded-circle position-absolute top-0 end-0">2</span>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                            <li><a class="dropdown-item" href="#">Action1</a></li>
                            <li><a class="dropdown-item" href="#">Another action1</a></li>
                            <li><a class="dropdown-item" href="#">Something else here1</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>
