<div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Create new tag</h5>
            </div>
            <div class="model-body m-4 ">
                {!! Form::open(['id'=>'tagForm']) !!}
                <div class="mb-3">
                    <input type="text" class="form-control h-35 border-dark" id="exampleFormControlInput1" placeholder="Enter Tag Name">
                    <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-outline-dark" data-bs-dismiss="modal">Create</button>
                        <button type="button" class="btn btn-dark">Cancel</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
    <div class="offcanvas-header">
        <h5 id="offcanvasRightLabel " class="fw-bold">Tag list</h5>
        <button  class="btn" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
            <h1>+</h1>
        </button>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div>
        <table class="table p-3 text-center">
            <tbody>
            <tr>
                <td class="border border-danger fw-bold fs-5 rounded-2">A</td>
                <td>Otto</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-outline" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-ellipsis"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#"><i class="fa-solid fa-pencil px-2"></i>Rename</a></li>
                            <li>
                                <button class="btn btn-outline dropdown-item"><i class="fa-solid fa-trash px-2"></i>Delete</button>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="border border-primary px-2 py-2 fw-bold fs-5 rounded-4">B</td>
                <td>Thornton</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-outline" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-ellipsis"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#"><i class="fa-solid fa-pencil px-2"></i>Rename</a></li>
                            <li>
                                <button class="btn btn-outline dropdown-item"><i class="fa-solid fa-trash px-2"></i>Delete</button>
                            </li>
                        </ul>
                    </div>
                </td>
            <tr>
                <td class="border border-info px-2 py-2 fw-bold fs-5 rounded-4">C</td>
                <td>Thornton</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-outline" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-ellipsis"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#"><i class="fa-solid fa-pencil px-2"></i>Rename</a></li>
                            <li>
                                <button class="btn btn-outline dropdown-item"><i class="fa-solid fa-trash px-2"></i>Delete</button>
                            </li>
                        </ul>
                    </div>
                </td>
            </tbody>
        </table>
    </div>
</div>
