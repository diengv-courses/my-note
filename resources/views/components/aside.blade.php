<div>
    <div class="border-bottom m-3 p-3">
        <i class=" logo las la-quran fs-5"></i>
        <label class="fs-5">NotePlus</label>
    </div>
    <div class="dropdown">
        <div class="user-dropdown  btn-lg dropdown-toggle 75 text-center" type="" id="dropdownMenuButton1"
                data-bs-toggle="dropdown" aria-expanded="false">
            <img style="" class="mr-4"
                 src="https://png.pngtree.com/png-clipart/20190520/original/pngtree-vector-users-icon-png-image_4144740.jpg"
                 alt="">
            <span class="w-75 pl-2">Xuân Hoàng</span>
        </div>
        <ul class="dropdown-menu w-100 mt-3" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item" href="resources/views/information/editprofile.blade.php">Accont Setting</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
        </ul>
    </div>

    <div class="search iq-search-bar device-search mx-3 mt-2 ">
        <form action="#" class="searchbox ml-4">
            <div class="form-group has-search">
                <i class="fa fa-search form-control-feedback"></i>
                <a href="#" class="form-control-feedback"><i class="fa-solid fa-magnifying-glass"></i></a>
                <input type="text" class="form-control border-0 bg-light w-80 align-content-center" placeholder="Search">
            </div>
        </form>
    </div>
    <div class="btn-group border-bottom m-3 pt-2 w-90">
        <button type="button" data-bs-toggle="dropdown"  class="btn btn-dark"><i class="fa-solid fa-plus px-4"></i>Add Note</button>
        <button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split px-4" id="dropdownMenuReference" data-bs-toggle="dropdown" aria-expanded="false" data-bs-reference="parent">
            <span class="visually-hidden">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuReference">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Separated link</a></li>
        </ul>
    </div>
    <div class="menu pt-6">
        <div>
            <p>
                <i class="las la-folder-open "></i>
                <span class="w-100 mr-5 position-relative" data-bs-toggle="collapse" href="#collapseExample"
                      role="button" aria-expanded="false" aria-controls="collapseExample">
    Notebooks
    <i class="las la-angle-down icon-notebook "></i>
  </span>

            </p>
            <div class="collapse" id="collapseExample" data-bs-spy="scroll">

                <div class="">
                    <a href="#" class="nav-link"><i class="las la-folder-open"></i>User List</a>
                </div><br>
                <div class="">
                    <a href="#" class="nav-link"><i class="las la-folder-open"></i>User List</a>
                </div><br>
                <div class="">
                    <a href="#" class="nav-link"><i class="las la-folder-open"></i>User List</a>
                </div><br>
            </div>
        </div>
        <div class="mt-3">
            <i class="las la-clock"></i>
            <span>Reminder</span>
        </div>
        <div style="cursor: pointer" class="mt-3">
            <i class="las la-clock"></i><span  data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" class="px-0">Tags List</span>
        </div>
        <div class="mt-3">
            <i class="las la-trash"></i>
            <span>Bin</span>

        </div>


</div>
    <div class=" mt-3 menu pt-6">
        <div>
            <p>
                <i class="las la-folder-open "></i>
                <span class="w-100 mr-5 position-relative" data-bs-toggle="collapse" href="#collapseExample1"
                      role="button" aria-expanded="false" aria-controls="collapseExample1">
   User Detail
    <i class="las la-angle-down icon-notebook "></i>
  </span>

            </p>
            <div class="collapse" id="collapseExample1" data-bs-spy="scroll">

                <div class="">

                    <a href="#" class="nav-link"><i class="las la-folder-open"></i>User Profile</a>
                </div><br>
                <div class="">
                    <a href="#" class="nav-link"><i class="las la-folder-open"></i>User Add</a>
                </div><br>
                <div class="">
                    <a href="#" class="nav-link"><i class="las la-folder-open"></i>User List</a>
                </div><br>
            </div>
        </div>
    </div>
    <div class="logo-upgrade text-center mb-3">
        <img width="200px"
             src="https://templates.iqonic.design/note-plus/html/assets/images/layouts/side-bkg.png"
             alt="">
        <p>Set Buisness Account To Explore Premiun Features</p>
        <button class="bg-dark text-white rounded border-0 p-2">Upgrade</button>
    </div>

</div>
