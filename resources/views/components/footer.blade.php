<div class="mt-5">
    <!-- Nothing in life is to be feared, it is only to be understood. Now is the time to understand more, so that we may fear less. - Marie Curie -->
    <div class="row">
        <!-- Nothing in life is to be feared, it is only to be understood. Now is the time to understand more, so that we may fear less. - Marie Curie -->
        <div class="row bg-white  rounded-start rounded-end  pl-2">
            <div class="col-lg-6  ">
                <ul class="d-flex ">
                    <li class="d-inline p-2">
                        Privacy Policy
                    </li>
                    <li class="d-inline p-2">
                        Terms of Use
                    </li>
                </ul>
            </div>
            <div class="col-lg-6">
                <ul class="d-flex justify-content-end">
                    <li class="d-inline p-2">
                        2023©
                    </li>
                    <li  class="d-inline p-2">
                        NotePlus
                    </li>
                </ul>

            </div>
        </div>

    </div>
</div>
