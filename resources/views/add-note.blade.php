<x-master-layout>
<div class="container-fluid bg-white">
    <div class="card-body write-card pb-4">
        <div class="row">
            <div class="col-md-8 mt-4">
                <form action="">
                    <div class="form-group mb-3">
                        <label class="label-control mb-3">Title</label>
                        <input type="text" class="form-control" name="title" placeholder="Example Note" value=""
                               data-change="input" data-custom-target="#note-title">
                    </div>
                    <div class="form-group mb-3">
                        <label class="label-control mb-2">Description</label>
                        <textarea type="text" class="form-control" name="description" rows="3" data-change="input"
                                  data-custom-target="#note-description"
                                  placeholder="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."></textarea>
                    </div>
                    <div class="form-group mb-3">
                        <label class="label-control mb-2">Reminder Date</label>
                        <input type="date" class="form-control" name="reminder_date" value="2021-01-01"
                               data-change="input" data-custom-target="#note-reminder-date">
                    </div>
                    <p>Icon</p>
                    <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                        <div class="mx-1">
                            <input type="radio" class="btn-check mx-2" name="btnradio" id="btnradio1" autocomplete="off"
                                   checked>
                            <label class="btn btn-outline-dark" for="btnradio1">
                                <svg width="23" class="svg-icon" xmlns="http://www.w3.org/2000/svg" fill="none"
                                     viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path>
                                </svg>
                            </label>
                        </div>
                        <div class="mx-1">
                            <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
                            <label class="btn btn-outline-dark" for="btnradio2">
                                <svg width="23" class="svg-icon" xmlns="http://www.w3.org/2000/svg" fill="none"
                                     viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01"></path>
                                </svg>
                            </label>
                        </div>
                        <div class="mx-1">
                            <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
                            <label class="btn btn-outline-dark" for="btnradio3">
                                <svg width="23" class="svg-icon" xmlns="http://www.w3.org/2000/svg" fill="none"
                                     viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M12 6V4m0 2a2 2 0 100 4m0-4a2 2 0 110 4m-6 8a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4m6 6v10m6-2a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4"></path>
                                </svg>
                            </label>
                        </div>
                        <div class="mx-1">
                            <input type="radio" class="btn-check" name="btnradio" id="btnradio4" autocomplete="off">
                            <label class="btn btn-outline-dark" for="btnradio4">
                                <svg width="23" class="svg-icon" xmlns="http://www.w3.org/2000/svg" fill="none"
                                     viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M5 8h14M5 8a2 2 0 110-4h14a2 2 0 110 4M5 8v10a2 2 0 002 2h10a2 2 0 002-2V8m-9 4h4"></path>
                                </svg>
                            </label>
                        </div>
                        <div class="mx-1">
                            <input type="radio" class="btn-check" name="btnradio" id="btnradio5" autocomplete="off">
                            <label class="btn btn-outline-dark" for="btnradio5">
                                <svg width="23" class="svg-icon" xmlns="http://www.w3.org/2000/svg" fill="none"
                                     viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                </svg>
                            </label>
                        </div>
                    </div>
                    <div class="form-group mt-4 mb-4">
                        <label class="label-control mb-2">Priority</label>
                        <div>
                            <select name="priority" id="" class="form-control" data-change="select" data-custom-target="color">
                                <option value="primary">Default</option>
                                <option value="success">Very Low</option>
                                <option value="info" selected="">Low</option>
                                <option value="warning">Medium</option>
                                <option value="danger">High</option>
                                <option value="purple">Very High</option>
                            </select>
                        </div>
                    </div>

                    <button type="reset" class="btn btn-outline-dark" data-reset="note-reset">
                        <svg width="20" class="svg-icon" id="new-note-reset" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12.066 11.2a1 1 0 000 1.6l5.334 4A1 1 0 0019 16V8a1 1 0 00-1.6-.8l-5.333 4zM4.066 11.2a1 1 0 000 1.6l5.334 4A1 1 0 0011 16V8a1 1 0 00-1.6-.8l-5.334 4z" style="stroke-dasharray: 56px, 76px; stroke-dashoffset: 0px;"></path>
                        </svg>
                        Reset
                    </button>
                    <button type="submit" class="btn btn-dark ml-1">
                        <svg width="20" class="svg-icon" id="new-note-save" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7H5a2 2 0 00-2 2v9a2 2 0 002 2h14a2 2 0 002-2V9a2 2 0 00-2-2h-3m-1 4l-3 3m0 0l-3-3m3 3V4" style="stroke-dasharray: 70px, 90px; stroke-dashoffset: 0px;"></path>
                        </svg>
                        Save
                    </button>
                </form>
            </div>
            <div class="col-md-4 mt-4" id="default">
                <div class="card card-block card-stretch  border-0 border-bottom border-primary note-detail h-100 rounded-0" id="update-note">
                    <div class="card-header d-flex justify-content-between border-0 pb-1 ">
                        <div class="icon iq-icon-box-2 icon-border-info border p-2 rounded-3 border-primary" id="note-icon">
                            <svg width="23" class="svg-icon " id="iq-main-01" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path class="text-primary" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" style="stroke-dasharray: 83px, 103px; stroke-dashoffset: 0px;"></path>
                            </svg>
                        </div>
                        <div class="card-header-toolbar d-flex align-items-center">
                            <div class="dropdown">
                                <span class="" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    ...
                                </span>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body border-0">
                        <h4 class="card-title text-ellipsis short-1" id="note-title">Example Note</h4>
                        <p class="mb-3 text-ellipsis  short-6" id="note-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                    <div class="card-footer border-0">
                        <div class="d-flex align-items-center justify-content-between note-text note-text-info">
                            <a href="#" class="text-decoration-none"><i class="las la-user-friends m-2 fs-5"></i><span class="text-dark fw-bold">Only Me</span> </a>
                            <a href="#" class="text-decoration-none"><i class="las la-calendar m-2 fs-5"></i><span  class="text-dark fw-bold" id="note-reminder-date">01 Jan 2021</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</x-master-layout>
