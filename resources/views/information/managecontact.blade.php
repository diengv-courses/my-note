<x-master-layout>
<div class="col-lg-12" style="padding: 20px">
    <div class="card" >
        <div class="card-body p-0">
            <div class="iq-edit-list usr-edit">
                <ul class="iq-edit-profile d-flex nav nav-pills">
                    <li class="col-md-3 p-0">
                        <a class="nav-link active" data-toggle="pill" href="{{route('editprofile')}}">
                            Personal Information
                        </a>
                    </li>
                    <li class="col-md-3 p-0">
                        <a class="nav-link" data-toggle="pill" href="{{route('changepassword')}}">
                            Change Password
                        </a>
                    </li>
                    <li class="col-md-3 p-0">
                        <a class="nav-link" data-toggle="pill" href="{{route('emailandsms')}}">
                            Email and SMS
                        </a>
                    </li>
                    <li class="col-md-3 p-0">
                        <a class="nav-link" data-toggle="pill" href="#">
                            Manage Contact
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div style="  padding: 20px !important;">
    <div class="card-header d-flex justify-content-between" style="  background: transparent;
    border-radius: 0;
    padding: 20px !important;
    margin-bottom: 0;
    -ms-flex-align: center !important;
    align-items: center !important;
    border-bottom: 1px solid #DCDDDF;">
        <div class="iq-header-title" >
            <h4 class="card-title">Manage Contact</h4>
        </div>
    </div>
</div>
<form class="row g-3" style="padding: 20px">
    <div class="col-md-12">
        <label for="inputpw" class="form-label">Contact Number:</label>
        <input type="password" class="form-control" id="inputpw">
    </div>

    <div class="col-md-12">
        <label for="newpw" class="form-label">Email:</label>
        <input type="password" class="form-control" id="newpw">
    </div>

    <div class="col-md-12">
        <label for="newpw" class="form-label">Url:</label>
        <input type="password" class="form-control" id="newpw">
    </div>

    <div class="col-md-12">
        <label for="vpw" class="form-label">Verify Password:</label>
        <input type="password" class="form-control" id="vpw">
    </div>


    <div class="col-12">
        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button type="submit" class="btn iq-bg-danger">Cancel</button>
    </div>
</form>
</x-master-layout>
