<x-master-layout>
<div class="col-lg-12" style="padding: 20px">
    <div class="card" >
        <div class="card-body p-0">
            <div class="iq-edit-list usr-edit">
                <ul class="iq-edit-profile d-flex nav nav-pills">
                    <li class="col-md-3 p-0">
                        <a class="nav-link active" data-toggle="pill" href={{route('editprofile')}}">
                            Personal Information
                        </a>
                    </li>
                    <li class="col-md-3 p-0">
                        <a class="nav-link" data-toggle="pill" href="{{route('changepassword')}}">
                            Change Password
                        </a>
                    </li>
                    <li class="col-md-3 p-0">
                        <a class="nav-link" data-toggle="pill" href="#">
                            Email and SMS
                        </a>
                    </li>
                    <li class="col-md-3 p-0">
                        <a class="nav-link" data-toggle="pill" href="{{route('managecontact')}}">
                            Manage Contact
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div style="  padding: 20px !important;">
    <div class="card-header d-flex justify-content-between" style="  background: transparent;
    border-radius: 0;
    padding: 20px !important;
    margin-bottom: 0;
    -ms-flex-align: center !important;
    align-items: center !important;
    border-bottom: 1px solid #DCDDDF;">
        <div class="iq-header-title" >
            <h4 class="card-title">Email and SMS</h4>
        </div>
    </div>
</div>
<form class="row g-3" style="padding: 20px">
    <div class="form-group row align-items-center">
        <label class="col-md-3 col-form-label" for="emailnotification">Email Notification:</label>
        <div class="col-md-9">
            <div class="form-check form-switch">
                <input type="checkbox" class="form-check-input" id="emailnotification" checked="">
                <label class="form-check-label" for="emailnotification"></label>
            </div>
        </div>
    </div>

    <div class="form-group row align-items-center">
        <label class="col-md-3 col-form-label" for="smsnotification">SMS Notification:</label>
        <div class="col-md-9">
            <div class="form-check form-switch">
                <input type="checkbox" class="form-check-input" id="smsnotification" checked="">
                <label class="form-check-label" for="smsnotification"></label>
            </div>
        </div>
    </div>



    <div class="form-group row align-items-center">
        <label class="col-md-3 col-form-label" for="npass">When To Email</label>
        <div class="col-md-9">
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="email01">
                <label class="form-check-label" for="email01">You have new notifications.</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="email02">
                <label class="form-check-label" for="email02">You're sent a direct message</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="email03" checked="">
                <label class="form-check-label" for="email03">Someone adds you as a connection</label>
            </div>
        </div>
    </div>

    <div class="form-group row align-items-center mt-4">
        <label class="col-md-3 col-form-label" for="npass">When To Escalate Emails</label>
        <div class="col-md-9">
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="email04">
                <label class="form-check-label" for="email04">Upon new order.</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="email05">
                <label class="form-check-label" for="email05">New membership approval</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="email06" checked="">
                <label class="form-check-label" for="email06">Member registration</label>
            </div>
        </div>
    </div>

    <div class="col-12">
        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button type="submit" class="btn iq-bg-danger">Cancel</button>
    </div>
</form>
</x-master-layout>

