<x-master-layout>
<div class="col-lg-12" style="padding: 20px">
    <div class="card" >
        <div class="card-body p-0">
            <div class="iq-edit-list usr-edit">
                <ul class="iq-edit-profile d-flex nav nav-pills">
                    <li class="col-md-3 p-0">
                        <a class="nav-link active" data-toggle="pill" href="#">
                            Personal Information
                        </a>
                    </li>
                    <li class="col-md-3 p-0">
                        <a class="nav-link" data-toggle="pill" href=">
                            Change Password
                        </a>
                    </li>
                    <li class="col-md-3 p-0">
                        <a class="nav-link" data-toggle="pill" href="">
                            Email and SMS
                        </a>
                    </li>
                    <li class="col-md-3 p-0">
                        <a class="nav-link" data-toggle="pill" href="">
                            Manage Contact
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div style="  padding: 20px !important;">
    <div class="card-header d-flex justify-content-between" style="  background: transparent;
    border-radius: 0;
    padding: 20px !important;
    margin-bottom: 0;
    -ms-flex-align: center !important;
    align-items: center !important;
    border-bottom: 1px solid #DCDDDF;">
        <div class="iq-header-title" >
            <h4 class="card-title">Change Password</h4>
        </div>
    </div>
</div>
<form class="row g-3" style="padding: 20px">

    <div class="col-md-12">
    <div class="profile-img-edit">
        <div class="crm-profile-img-edit">
            <img src="https://impactive.id/themes/web/doimpact/assets/img/avatar-none.jpg" class="crm-profile-pic avatar-100" style="height: 100px;width: 100px;    line-height: 100px;min-width: 100px;">
            <div class="crm-p-image bg-primary" style="position: absolute;
    top: 159px;
    left: 90px;
    transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
    height: 35px;
    width: 35px;
    text-align: center;
    font-size: 12px;
    cursor: pointer;
    border: 5px solid #fff; ;
    ">
                <i class="fa-solid fa-pen-to-square" style="height: 13px;width: 13px; font-size: 13px;
    line-height: 26px;">   <input class="bg-white" type="file" accept="image/*"></i>

            </div>
        </div>

    </div>
    </div>
    <div class="col-md-6">
        <label for="inputfname" class="form-label">First Name:</label>
        <input type="text" class="form-control" id="inputfname">
    </div>
    <div class="col-md-6">
        <label for="lname" class="form-label">Last Name:</label>
        <input type="text" class="form-control" id="lname">
    </div>

    <div class="col-md-6">
        <label for="uname" class="form-label">User Name:</label>
        <input type="text" class="form-control" id="uname">
    </div>
    <div class="col-md-6">
        <label for="city" class="form-label">City:</label>
        <input type="text" class="form-control" id="city">
    </div>

    <div class= "form-group col-sm-6">
    <label class="d-block" for="inlineRadio1">Gender:</label>
    <div class="custom-control custom-radio custom-control-inline">

        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
        <label class="form-check-label" for="inlineRadio1">Male</label>
    </div>
    <div class="custom-control custom-radio custom-control-inline">
        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
        <label class="form-check-label" for="inlineRadio2">Female</label>
    </div>
    </div>

    <div class="col-6">
        <label for="inputbd" class="form-label">Date Of Birth:</label>
        <input type="date" class="form-control" id="inputbd" placeholder="Apartment, studio, or floor">
    </div>





    <div class="col-md-6">
        <label for="" class="form-label">Marital Status:</label>
        <select class="form-select" aria-label="Default select example">
        <option selected>Single</option>
        <option value="1">Married</option>
        <option value="2">Widowed</option>
        <option value="3">Divorced</option>
            <option value="3">Separated</option>
    </select>
    </div>

    <div class="col-md-6">
        <label for="" class="form-label">Age:</label>
        <select class="form-select" aria-label="Default select example">
            <option selected>33-45</option>
            <option value="1">12-18</option>
            <option value="2">19-32</option>
            <option value="3">33-45</option>
            <option value="3">46-62</option>
            <option value="3">63></option>
        </select>
    </div>

    <div class="col-md-6">
        <label for="" class="form-label">Country:</label>
        <select class="form-select" aria-label="Default select example">
            <option selected>USA</option>
            <option value="1">Caneda</option>
            <option value="2">Noida</option>
            <option value="3">USA</option>
            <option value="3">India</option>
            <option value="3">Africa</option>
        </select>
    </div>

    <div class="col-md-6">
        <label for="" class="form-label">State:</label>
        <select class="form-select" aria-label="Default select example">
            <option selected>Georgia</option>
            <option value="1">Califprnia</option>
            <option value="2">Florida</option>
            <option value="3">Georgia</option>
            <option value="3">Connecticut</option>
            <option value="3">louisiana</option>
        </select>
    </div>

    <div class="mb-12">
        <label for="exampleFormControlTextarea1" class="form-label">Example textarea</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">
                                       37 Cardinal Lane
                                       Petersburg, VA 23803
                                       United States of America
                                       Zip Code: 85001</textarea>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button type="submit" class="btn iq-bg-danger">Cancel</button>
    </div>
</form>
</x-master-layout>
