<x-master-layout>
<div class="p-3">
        <table class="table -drak w-50 m-20 border">
            <thead>
            <tr class="table-dark">
                <th scope="col">Title</th>
                <th scope="col">Created by</th>
                <th scope="col">Update</th>
                <th scope="col">Share with</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td><a href="">
                        <button class="btn btn-success px-2 border opacity-75"><i class="fa-solid fa-pencil"></i></button></a>
                    <button class="btn btn-danger border opacity-75"><i class="fa-solid fa-trash"></i></button>
                </td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                <td><a href="">
                        <button class="btn btn-success px-2 border opacity-75"><i class="fa-solid fa-pencil"></i></button></a>
                    <button class="btn btn-danger border opacity-75"><i class="fa-solid fa-trash"></i></button>
                </td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td colspan="2">Larry the Bird</td>
                <td>@twitter</td>
                <td><a href="">
                        <button class="btn btn-success px-2 border opacity-75"><i class="fa-solid fa-pencil"></i></button></a>
                    <button class="btn btn-danger border opacity-75"><i class="fa-solid fa-trash"></i></button>
                </td>
            </tr>

            </tbody>
        </table>
    </div>
</x-master-layout>
