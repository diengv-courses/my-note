<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function index (){
        return view('notes.index');
    }
    public function create (){
        return view('notes.add');
    }
    public function store (){

    }
    public function edit ($id){

    }
    public function update ($id){

    }
    public function destroy ($id){

    }
}
