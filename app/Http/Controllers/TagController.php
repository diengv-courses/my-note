<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index (){
        return view('tags.index');
    }
    public function create (){
        return view('tags.add');
    }
    public function store (){

    }
    public function edit ($id){

    }
    public function update ($id){

    }
    public function destroy ($id){

    }
}
