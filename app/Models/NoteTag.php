<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoteTag extends Model
{
    use HasFactory;
    use SoftDeletes;
    public  $table='note_tags';
    protected $fillable = [
      'note_id',
        'tag_id'

    ];
    public function tag(){
        return $this->belongsTo(Tag::class,'tag_id','id');
    }
    public function note(){
        return $this->belongsTo(Note::class,'note_id','id');
    }
}
