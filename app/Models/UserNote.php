<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNote extends Model
{
    use HasFactory;
    use SoftDeletes;
    public  $table='user_notes';
    protected $fillable = [
       'node_id',
        'user_id',
        'created_type',
        'can_edit',
        'is_own',
        'is_favourite',
        'is_pin',
        'is_access'
    ];
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function note(){
        return $this->belongsTo(Note::class,'note_id','id');
    }
}
