<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    use HasFactory;
    use SoftDeletes;
    public  $table='notes';
    protected $fillable = [
        'title',
        'desc',
        'content',
        'category_id',
        'password',
        'todo_item',
        'remind_time',
        'icon',
        'priority',
        'has_id',
        'deleted_at'
    ];

    public static $priorityValues = [
        'high' => 'Height',
        'medium' => 'Medium',
        'low' => 'Low',
    ];

    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }
    public function note_tags(){
        return $this->hasMany(NoteTag::class,'note_id','id');
    }
    public function user_notes(){
        return $this->hasMany(UserNote::class,'node_id','id');
    }
}
