<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_notes', function (Blueprint $table) {
            $table->id();
            $table->uuid('note_id');
            $table->integer('user_id');
            $table->string('created_type',255);
            $table->boolean('can_edit');
            $table->boolean('is_own');
            $table->boolean('is_favourite');
            $table->boolean('is_pin');
            $table->boolean('is_access');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_notes');
    }
};
