<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title',255);
            $table->text('desc')->nullable();
            $table->string('content',255);
            $table->uuid('category_id');
            $table->string('password',20)->nullable();
            $table->json('todo_item')->nullable();
            $table->date('remind_time')->nullable();
            $table->string('icon',255)->nullable();
            $table->enum('priority', ['high', 'medium', 'low'])->nullable();
            $table->string('has_id',255)->unique();
            $table->date('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notes');
    }
};
