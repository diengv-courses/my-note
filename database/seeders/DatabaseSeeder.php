<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\NoteTag;
use App\Models\User;
use App\Models\UserNote;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory(10)->create();
        Category::factory()->count(10)->create();
        UserNote::factory(10)->create();
        NoteTag::factory(10)->create();
    }
}
