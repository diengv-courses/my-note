<?php

namespace Database\Factories;

use App\Models\Note;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserNote>
 */
class UserNoteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'note_id' => Note::factory()->create()->id,
            'user_id' => User::factory()->create()->id ,
            'created_type' => $this->faker->word(),
            'can_edit' => $this->faker->boolean(),
            'is_own' => $this->faker->boolean(),
            'is_favourite' => $this->faker->boolean(),
            'is_pin' => $this->faker->boolean(),
            'is_access' => $this->faker->boolean(),
        ];
    }
}
