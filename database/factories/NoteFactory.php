<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Note;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Note>
 */
class NoteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $categoryId = DB::table('categories')
            ->inRandomOrder()
            ->select('id')
            ->first()
            ->id;

        return [
            'id' => Uuid::uuid4()->toString(),
            'title' => $this->faker->sentence(),
            'desc' => $this->faker->paragraph(),
            'content' => $this->faker->text(),
            'category_id' => $categoryId,
            'password' => $this->faker->password(),
            'todo_item' => json_encode([$this->faker->sentence(), $this->faker->sentence()]),
            'remind_time' => $this->faker->dateTime(),
            'icon' => $this->faker->imageUrl(),
            'priority' => $this->faker->randomElement(array_keys(Note::$priorityValues)),
            'has_id' => Str::random()

        ];
    }
}
