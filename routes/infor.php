<?php

Route::get('/information/editprofile',function (){
    return view('/information/editprofile');
})->name('editprofile');
Route::get('/information/changepassword',function (){
    return view('/information/changepassword');
})->name('changepassword');
Route::get('/information/emailandsms',function (){
    return view('/information/emailandsms');
})->name('emailandsms');
Route::get('/information/managecontact',function (){
    return view('/information/managecontact');
})->name('managecontact');

Route::get('/information/accountsettings',function (){
    return view('/information/accountsettings');
})->name('accountsettings');

Route::get('/information/privacysetting',function (){
    return view('/information/privacysetting');
})->name('privacysetting');

