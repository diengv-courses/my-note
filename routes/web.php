<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\AuthController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
//Route::group(['prefix' =>'auth'], function () {
//
//    Route::get('/login', [AuthController::class, 'index'])->name('list');
//    Route::get('/create', [AuthController::class, 'create'])->name('create');
//    Route::post('/store', [AuthController::class, 'store'])->name('store');
//    Route::get('/edit', [AuthController::class, 'edit'])->name('edit');
//    Route::get('/register', [AuthController::class, 'register'])->name('register');
//    Route::post('/update', [AuthController::class, 'update'])->name('update');
//    Route::get('/destroy', [AuthController::class, 'destroy'])->name('destroy');
//
//});
//
//Route::group(['prefix' =>'user'], function () {
//
//    Route::get('/', [UserController::class, 'index'])->name('list');
//    Route::get('/create', [UserController::class, 'create'])->name('create');
//    Route::post('/store', [UserController::class, 'store'])->name('store');
//    Route::get('/edit', [UserController::class, 'edit'])->name('edit');
//    Route::post('/update', [UserController::class, 'update'])->name('update');
//    Route::get('/destroy', [UserController::class, 'destroy'])->name('destroy');
//
//});
//Route::group(['prefix' =>'tag'], function () {
//
//    Route::get('/', [TagController::class, 'index'])->name('list');
//    Route::get('/create', [TagController::class, 'create'])->name('create');
//    Route::post('/store', [TagController::class, 'store'])->name('store');
//    Route::get('/edit', [TagController::class, 'edit'])->name('edit');
//    Route::post('/update', [TagController::class, 'update'])->name('update');
//    Route::get('/destroy', [TagController::class, 'destroy'])->name('destroy');
//
//});
//Route::group(['prefix' =>'note'], function () {
//
//    Route::get('/', [NoteController::class, 'index'])->name('list');
//    Route::get('/create', [NoteController::class, 'create'])->name('create');
//    Route::post('/store', [NoteController::class, 'store'])->name('store');
//    Route::get('/edit', [NoteController::class, 'edit'])->name('edit');
//    Route::post('/update', [NoteController::class, 'update'])->name('update');
//    Route::get('/destroy', [NoteController::class, 'destroy'])->name('destroy');
//
//});
//Route::group(['prefix' =>'category'], function () {
//
//    Route::get('/', [CategoryController::class, 'index'])->name('list');
//    Route::get('/create', [CategoryController::class, 'create'])->name('create');
//    Route::post('/store', [CategoryController::class, 'store'])->name('store');
//    Route::get('/edit', [CategoryController::class, 'edit'])->name('edit');
//    Route::post('/update', [CategoryController::class, 'update'])->name('update');
//    Route::get('/destroy', [CategoryController::class, 'destroy'])->name('destroy');
//
//});
Route::get('/',function (){
    return view('notes.add');
});


