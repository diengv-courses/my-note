<?php
use Illuminate\Support\Facades\Route;
Route::group(['prefix'=>'auth'],function (){
    Route::get('/login', function () {
        return view('auth.login');
    });
    Route::get('/register', function () {
        return view('auth.register');
    });
    Route::get('/recover', function () {
        return view('auth.recover');
    });
    Route::get('/confirm', function () {
        return view('auth.confirm');
    });
    Route::get('/lock', function () {
        return view('auth.lock');
    });

});
